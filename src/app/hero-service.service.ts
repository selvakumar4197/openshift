import { Injectable } from '@angular/core';
import { Hero } from './hero';
import { valuetostore } from './herovalue';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';
import  { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})


// Get the data fronm the local ./herovalue 

// export class HeroServiceService {
//   gethero(): Observable<Hero[]> {
//     return of(valuetostore);
//   }

//   constructor() { }
// }


// Get the data from the Mongo DB & msSql
export class HeroServiceService {
    gethero() {
      return this.http.get('http://localhost:8800/data',{})
    }
  
    constructor(private http: HttpClient) { }
  }
