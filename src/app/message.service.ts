import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  message: string[] = ["Selva", "Kumar", "Nallasamy"];

  add (message){
    this.message.push(message.name);
  }

  clear (){
    this.message = [];
  }

  constructor() { }
}
