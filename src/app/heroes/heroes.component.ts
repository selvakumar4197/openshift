import { Component, OnInit } from '@angular/core';
import {Hero} from '../hero';
import {valuetostore} from '../herovalue'
import { HeroServiceService } from '../hero-service.service';
import { MessageService } from '../message.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss'],
})
export class HeroesComponent implements OnInit {
  // heroes: Hero[]= valuetostore;
  selectedHero: Hero;
  
  onSelect(hero){
  this.selectedHero = hero;
  this.MessageService.add(hero);
}

heroes;
getHeroes(){
  this.heroService.gethero().subscribe( (heroes : any) =>
     this.heroes = heroes.recordset
     );
}

  constructor(private heroService : HeroServiceService,private MessageService: MessageService) { }

  ngOnInit() {
    this.getHeroes();
  }

}
